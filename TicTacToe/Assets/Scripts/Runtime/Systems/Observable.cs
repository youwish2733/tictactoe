using System;

/// <summary>
/// Wraps a value to observe its modifications.
/// </summary>
public class Observable<T>
{
    private T internalValue;

    /// <summary>
    /// Returns the previous value of the observed value.
    /// </summary>
    public T PreviousValue { get; private set; }

    /// <summary>
    /// Returns whether the value has been changed or not.
    /// </summary>
    public bool IsSet { get; private set; }

    public Observable()
    {
        internalValue = typeof(T) == typeof(string) ? (T)(object)string.Empty : default;
    }

    public Observable(T internalValue)
    {
        this.internalValue = internalValue;
    }

    /// <summary>
    /// Action listener for changed event. 
    /// This event will be triggered when the internal value is modified.
    /// </summary>
    public event Action<T> Changed;

    /// <summary>
    /// Adds a listener for on changed event.
    /// </summary>
    /// <param name="listener"></param>
    public void AddListener(Action<T> listener)
    {
        Changed += listener;
    }

    /// <summary>
    /// Clears all listeners.
    /// </summary>
    public void RemoveListeners()
    {
        Changed = null;
    }

    /// <summary>
    /// Exposes the internal value.
    /// </summary>
    public T Value
    {
        get => internalValue;
        set
        {
            if (value != null && value.Equals(internalValue))
            {
                return;
            }

            var oldValue = internalValue;
            internalValue = value;
            PreviousValue = oldValue;
            Changed?.Invoke(value);
        }
    }

    /// <summary>
    /// Sets the internal value.
    /// </summary>
    /// <param name="value"></param>
    public void Set(T value)
    {
        Value = value;
        IsSet = true;
    }
}