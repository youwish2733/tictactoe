using TicTacToe;
using UnityEngine;

public class Game : MonoBehaviour
{
    // Singleton for the Game class
    public static Game Instance
    {
        get
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    var go = new GameObject();
                    go.name = "Game";
                    instance = go.AddComponent<Game>();

                    DontDestroyOnLoad(go);
                }
            }

            return instance;
        }
    }

    private static Game instance = null;

    public ViewModelManager ViewModelManager => ViewModelManager.Instance;
    public ScreenManager ScreenManager => ScreenManager.Instance;
}
