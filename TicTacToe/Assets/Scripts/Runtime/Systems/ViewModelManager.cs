using System;
using System.Collections.Generic;
using UnityEngine;

public class ViewModelManager : MonoBehaviour
{
    // Singleton for the ViewModelManager class
    public static ViewModelManager Instance
    {
        get
        {
            if (instance == null)
            {
                if (instance == null)
                {
                    var go = new GameObject();
                    go.name = "ModelRepository";
                    instance = go.AddComponent<ViewModelManager>();

                    DontDestroyOnLoad(go);
                }
            }

            return instance;
        }
    }

    private static ViewModelManager instance = null;

    private Dictionary<Type, ViewModel> models { get; set; } = new Dictionary<Type, ViewModel>();

    /// <summary>
    /// Returns the model associated with the specified type or adds a new one if it doesn't exist.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T Get<T>() where T : ViewModel, new()
    {
        if (models.ContainsKey(typeof(T)))
        {
            return models[typeof(T)] as T;
        }
        var vm = new T();
        models.Add(typeof(T), vm);
        return vm;
    }
}