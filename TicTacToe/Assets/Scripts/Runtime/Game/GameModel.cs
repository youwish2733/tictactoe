using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public enum Difficulty
{
    Easy,
    Medium,
    Hard
}

public enum PlayerType
{
    None = 0,
    Player = 1,
    AI = 2
}

public class GameModel
{
    public PlayerType CurrentPlayer => currentPlayer;
    public PlayerType[] Board => board;
    public PlayerType Winner => GetWinner(Board);

    private int difficulty = 3;
    private PlayerType currentPlayer = PlayerType.Player;
    private PlayerType startingPlayer = PlayerType.Player;
    private PlayerType[] board = new PlayerType[9];

    private GameViewModel gameViewModel;

    public GameModel()
    {
        gameViewModel = Game.Instance.ViewModelManager.Get<GameViewModel>();
    }

    /// <summary>
    /// Sets the game's difficulty.
    /// </summary>
    /// <param name="difficulty"></param>
    public void SetPlayer(PlayerType player)
    {
        startingPlayer = player;
        currentPlayer = startingPlayer;
    }

    /// <summary>
    /// Sets the game's difficulty.
    /// </summary>
    /// <param name="difficulty"></param>
    public void SetDifficulty(Difficulty difficulty)
    {
        switch (difficulty)
        {
            case Difficulty.Easy:
                this.difficulty = 5;
                break;
            case Difficulty.Medium:
                this.difficulty = 7;
                break;
            case Difficulty.Hard:
                this.difficulty = 9;
                break;
            default:
                this.difficulty = 5;
                break;
        }
    }

    /// <summary>
    /// Marks the given tile.
    /// </summary>
    /// <param name="tileIndex"></param>
    public void MakeMove(int tileIndex)
    {
        board[tileIndex] = currentPlayer;
    }

    /// <summary>
    /// Returns whether the game is over or not.
    /// </summary>
    /// <returns></returns>
    public bool IsGameOver()
    {
        var winner = GetWinner(board);
        if (winner != PlayerType.None)
        {
            if (winner == PlayerType.AI)
            {
                gameViewModel.AIScore++;
            }
            else
            {
                gameViewModel.PlayerScore++;
            }

            return true;
        }

        if (IsBoardFull(board))
        {
            return true;
        }

        return false;
    }

    /// <summary>
    /// Switches the current player.
    /// </summary>
    public void SwitchPlayer()
    {
        var playerint = (3 - (int)currentPlayer);
        currentPlayer =  (PlayerType)playerint; // switch player (1->2, 2->1)
    }

    /// <summary>
    /// Returns the winner, if available.
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    private PlayerType GetWinner(PlayerType[] board)
    {
        int[,] winningPositions = new int[8, 3]
        {
            {0, 1, 2}, {3, 4, 5}, {6, 7, 8}, // rows
            {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, // columns
            {0, 4, 8}, {2, 4, 6} // diagonals
        };

        for (int i = 0; i < winningPositions.GetLength(0); i++)
        {
            int a = winningPositions[i, 0];
            int b = winningPositions[i, 1];
            int c = winningPositions[i, 2];

            if (board[a] != 0 && board[a] == board[b] && board[b] == board[c])
            {
                return board[a];
            }
        }

        return 0; // no winner
    }

    /// <summary>
    /// Returns whether the board is filled or not.
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    public bool IsBoardFull(PlayerType[] board)
    {
        for (int i = 0; i < board.Length; i++)
        {
            if (board[i] == 0)
            {
                return false;
            }
        }
        return true;
    } 
    
    /// <summary>
    /// Returns whether the board is empty or not.
    /// </summary>
    /// <param name="board"></param>
    /// <returns></returns>
    public bool IsBoardEmpty(PlayerType[] board)
    {
        for (int i = 0; i < board.Length; i++)
        {
            if (board[i] != 0)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Returns the index of the tile that the AI chooses.
    /// </summary>
    /// <returns></returns>
    public int GetAIMove()
    {
        var newBoard = (PlayerType[])board.Clone();
        int bestScore = int.MinValue;
        int bestMove = -1;

        if (startingPlayer == PlayerType.AI && IsBoardEmpty(newBoard))
        {
            var rand = new Random();
            int randomIndex = rand.Next(newBoard.Length);
            return randomIndex;
        }

        var indexList = new List<int>();

        for (int i = 0; i < newBoard.Length; i++)
        {
            if (newBoard[i] == 0)
            {
                newBoard[i] = PlayerType.AI; // AI's move
                int score = Minimax(newBoard, difficulty, false);
                newBoard[i] = 0; // undo move

                if (bestScore == score)
                {
                    indexList.Add(i);
                }
                else if (score > bestScore)
                {
                    if (indexList.Count > 0)
                    {
                        indexList.Clear();
                    }
                    bestScore = score;
                    bestMove = i;
                }
            }
        }

        // Add randomization to the AI. If it finds more than 1 equally optimal move, it should pick one at random instead of the first one.
        // The indexList holds all equally optimal moves. This list is used to optimize the randomization by avoiding randomizing every time an equally optimal solution is found.
        if (indexList.Count > 0)
        {
            var random = new Random();
            var index = random.Next(0, indexList.Count);
            bestMove = Math.Max(bestMove, indexList[index]);
        }

        return bestMove;
    }

    /// <summary>
    /// Computes Tic Tac Toe AI move.
    /// </summary>
    /// <param name="board"></param>
    /// <param name="depth"></param>
    /// <param name="isMaximizingPlayer"></param>
    /// <returns></returns>
    private int Minimax(PlayerType[] board, int depth, bool isMaximizingPlayer)
    {
        var winner = GetWinner(board);
        if (winner != PlayerType.None || depth == 0)
        {
            return winner == PlayerType.AI ? 10 - depth : depth - 10; // AI wins -> high score, player wins -> low score
        }

        if (IsBoardFull(board))
        {
            return 0; // tie
        }

        if (isMaximizingPlayer)
        {
            int bestScore = int.MinValue;
            for (int i = 0; i < board.Length; i++)
            {
                if (board[i] == 0)
                {
                    board[i] = PlayerType.AI; // AI's move
                    int score = Minimax(board, depth - 1, false);
                    board[i] = 0; // undo move
                    bestScore = Mathf.Max(bestScore, score);
                }
            }
            return bestScore;
        }
        else
        {
            int bestScore = int.MaxValue;
            for (int i = 0; i < board.Length; i++)
            {
                if (board[i] == 0)
                {
                    board[i] = PlayerType.Player; // player's move
                    int score = Minimax(board, depth - 1, true);
                    board[i] = 0; // undo move
                    bestScore = Mathf.Min(bestScore, score);
                }
            }
            return bestScore;
        }
    }

    /// <summary>
    /// Resets the game.
    /// </summary>
    public void Reset()
    {
        currentPlayer = startingPlayer;

        for (int i = 0; i < board.Length; i++)
        {
            board[i] = 0;
        }
    }
}
