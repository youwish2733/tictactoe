public class GameController
{
    private readonly GameModel model;
    private readonly GameScreen screen;

    public GameController(GameModel model, GameScreen screen)
    {
        this.model = model;
        this.screen = screen;
    }

    public void StartGame()
    {
        if (model.CurrentPlayer == PlayerType.AI)
        {
            int aiMove = model.GetAIMove();
            OnTileClicked(aiMove);
        }
    }

    /// <summary>
    /// Sets the game's difficulty.
    /// </summary>
    /// <param name="difficulty"></param>
    public void SetDifficulty(Difficulty difficulty)
    {
        model.SetDifficulty(difficulty);
    }

    /// <summary>
    /// Sets the game's player.
    /// </summary>
    /// <param name="player"></param>
    public void SetPlayer(PlayerType player)
    {
        model.SetPlayer(player);
    }

    /// <summary>
    /// Manages the model and view when a player clicks on a tile.
    /// </summary>
    /// <param name="index"></param>
    public void OnTileClicked(int index)
    {
        if (model.IsGameOver() || model.Board[index] != 0)
        {
            return;
        }

        model.MakeMove(index);

        screen.SetTileIcon(index, model.CurrentPlayer);

        if (model.IsGameOver())
        {
            var winner = model.Winner;
            screen.EndGame(winner);
        }
        else
        {
            model.SwitchPlayer();
            if (model.CurrentPlayer == PlayerType.AI)
            {
                int aiMove = model.GetAIMove();
                OnTileClicked(aiMove);
            }
        }
    }

    /// <summary>
    /// Resets the game.
    /// </summary>
    public void ResetGame()
    {
        model.Reset();
        screen.ResetGame();
    }
}