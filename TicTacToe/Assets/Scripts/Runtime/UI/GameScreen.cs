using TicTacToe;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameScreen : BaseScreen
{
    [SerializeField] private Button resetButton;
    [SerializeField] private Button highscoreButton;
    [SerializeField] private Button playerButton;
    [SerializeField] private Image playerIcon;
    [SerializeField] private TextMeshProUGUI winnerText;
    [SerializeField] private TextMeshProUGUI startText;
    [SerializeField] private GridLayoutGroup grid;
    [SerializeField] private Button easyButton;
    [SerializeField] private Button mediumButton;
    [SerializeField] private Button hardButton;
    [SerializeField] private Transform difficultyPanel;
    [SerializeField] private Transform raycastBlocker;

    private Button[] tiles;
    private Sprite xSprite;
    private Sprite oSprite;
    private PlayerType startingPlayer = PlayerType.Player;
    private GameController gameController;

    protected override void Start()
    {
        base.Start();

        tiles = grid.GetComponentsInChildren<Button>();

        xSprite = Resources.Load<Sprite>("Icons/x");
        oSprite = Resources.Load<Sprite>("Icons/o");

        for (int i = 0; i < tiles.Length; i++)
        {
            // Avoid passing incorrect value as parameter
            var index = i;
            tiles[i].onClick.AddListener(() => gameController.OnTileClicked(index));
        }

        gameController = new GameController(new GameModel(), this);

        highscoreButton.onClick.AddListener(() => ScreenManager.Show<HighscoreScreen>());
        resetButton.onClick.AddListener(StartGame);
        playerButton.onClick.AddListener(SwitchPlayer);
        easyButton.onClick.AddListener(() => SelectDifficulty(Difficulty.Easy));
        mediumButton.onClick.AddListener(() => SelectDifficulty(Difficulty.Medium));
        hardButton.onClick.AddListener(() => SelectDifficulty(Difficulty.Hard));

        winnerText.text = "";
    }

    /// <summary>
    /// Sets the game's difficulty.
    /// </summary>
    /// <param name="difficulty"></param>
    public void SelectDifficulty(Difficulty difficulty)
    {
        gameController.SetDifficulty(difficulty);
    }

    /// <summary>
    /// Starts the game.
    /// </summary>
    public void StartGame()
    {
        startText.SetText("Reset");
        resetButton.onClick.RemoveAllListeners();
        resetButton.onClick.AddListener(gameController.ResetGame);
        ResetGame();
    }

    /// <summary>
    /// Switch the player.
    /// </summary>
    public void SwitchPlayer()
    {
        if (startingPlayer == PlayerType.Player)
        {
            startingPlayer = PlayerType.AI;
            playerIcon.sprite = oSprite;
        }
        else
        {
            startingPlayer = PlayerType.Player;
            playerIcon.sprite = xSprite;
        }
        gameController.SetPlayer(startingPlayer);
    }

    /// <summary>
    /// Ends the game.
    /// </summary>
    /// <param name="winner"></param>
    public void EndGame(PlayerType winner)
    {
        ShowWinnerMessage(winner == PlayerType.None ?
            "Tie!" :
            winner == PlayerType.Player ? "Player wins!" : "AI wins!");
        raycastBlocker.gameObject.SetActive(true);
        difficultyPanel.gameObject.SetActive(true);
        playerButton.gameObject.SetActive(true);
    }

    /// <summary>
    /// Sets the given tile with an icon based on which player's turn is.
    /// </summary>
    /// <param name="tileIndex"></param>
    /// <param name="player"></param>
    public void SetTileIcon(int tileIndex, PlayerType player)
    {
        var tileIcon = tiles[tileIndex].GetComponent<Image>();

        tileIcon.enabled = true;
        if (player == startingPlayer)
        {
            tileIcon.sprite = xSprite;
        }
        else
        {
            tileIcon.sprite = oSprite;
        }
    }

    /// <summary>
    /// Display the winner text.
    /// </summary>
    /// <param name="winnerText"></param>
    private void ShowWinnerMessage(string winnerText)
    {
        this.winnerText.text = winnerText;
    }

    /// <summary>
    /// Resets the game.
    /// </summary>
    public void ResetGame()
    {
        difficultyPanel.gameObject.SetActive(false);
        raycastBlocker.gameObject.SetActive(false);
        playerButton.gameObject.SetActive(false);
        for (int i = 0; i < tiles.Length; i++)
        {
            var tileIcon = tiles[i].GetComponent<Image>();
            tileIcon.sprite = null;
            tileIcon.enabled = false;
        }

        winnerText.text = "";
        gameController.StartGame();
    }
}