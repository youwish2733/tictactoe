using System;
using System.Collections;
using TicTacToe;
using UnityEngine;

public class BaseScreen : MonoBehaviour
{
    protected ScreenManager ScreenManager => ScreenManager.Instance;
    protected ViewModelManager ViewModelManager => ViewModelManager.Instance;

    protected virtual void Awake()
    {

    }

    protected virtual void Start()
    {
        
    }

    protected virtual void Update()
    {

    }

    /// <summary>
    /// Handles behavior when the screen is displayed.
    /// </summary>
    public virtual void Display()
    {

    }

    /// <summary>
    /// Stretches the screen's recttransform.
    /// </summary>
    public void Stretch()
    {
        var rectTransform = transform as RectTransform;

        // Set the anchors to the corners of the parent RectTransform
        rectTransform.anchorMin = Vector2.zero;
        rectTransform.anchorMax = Vector2.one;

        // Set the offset to zero
        rectTransform.offsetMin = Vector2.zero;
        rectTransform.offsetMax = Vector2.zero;
    }

    /// <summary>
    /// Calls an action with a delay.
    /// </summary>
    /// <param name="callBack"></param>
    /// <param name="delay"></param>
    protected void Call(Action callBack, float delay)
    {
        StartCoroutine(DelayedMethod(callBack, delay));
    }

    private IEnumerator DelayedMethod(Action callBack, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        callBack?.Invoke();
    }
}
