using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HighscoreScreen : BaseScreen
{
    [SerializeField] private Button backButton;
    [SerializeField] private TextMeshProUGUI playerScore;
    [SerializeField] private TextMeshProUGUI aiScore;

    private GameViewModel gameViewModel;
    protected override void Start()
    {
        base.Start();

        gameViewModel = ViewModelManager.Get<GameViewModel>();
        backButton.onClick.AddListener(() => ScreenManager.Show<GameScreen>());
    }

    public override void Display()
    {
        base.Display();

        playerScore.SetText(gameViewModel.PlayerScore.ToString());
        aiScore.SetText(gameViewModel.AIScore.ToString());
    }
}
