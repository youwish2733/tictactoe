using TicTacToe;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private ScreenManager screenManager => ScreenManager.Instance;

    [SerializeField] private GameScreen gameScreen;
    [SerializeField] private HighscoreScreen highscoreScreen;

    private void Start()
    { 
        screenManager.Wire(gameScreen);
        screenManager.Wire(highscoreScreen);
        screenManager.StartWith(gameScreen);
    }
}