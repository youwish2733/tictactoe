using System;
using System.Collections.Generic;
using UnityEngine;

namespace TicTacToe
{
    public class ScreenManager : MonoBehaviour
    {
        // Singleton for the ScreenManager
        public static ScreenManager Instance
        {
            get
            {
                if (instance == null)
                {
                    if (instance == null)
                    {
                        var go = new GameObject();
                        go.name = "ScreenManager";
                        instance = go.AddComponent<ScreenManager>();

                        DontDestroyOnLoad(go);
                    }
                }

                return instance;
            }
        }

        private static ScreenManager instance = null;

        private Dictionary<Type, BaseScreen> screens = new Dictionary<Type, BaseScreen>();

        public BaseScreen ActiveScreen;

        private void Awake()
        {
            InitInstance();
        }

        private void InitInstance()
        {
            instance = this;
        }

        /// <summary>
        /// Sets the given screen as active and displays it.
        /// </summary>
        /// <param name="screen"></param>
        public void StartWith(BaseScreen screen)
        {
            ActiveScreen = screen;
            screen?.gameObject.SetActive(true);
            screen.Stretch();
            screen.Display();
        }

        /// <summary>
        /// Registers and hides the given screen.
        /// </summary>
        /// <param name="screen"></param>
        public void Wire(BaseScreen screen)
        {
            if (screens.ContainsValue(screen))
            {
                return;
            }

            if (screen == null)
            {
                Debug.LogWarning("Cannot wire a null screen!");
                return;
            }

            screen.gameObject.SetActive(false);
            screens.Add(screen.GetType(), screen);
        }

        /// <summary>
        /// Presents screen of type T, if registered.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void Show<T>() where T : BaseScreen
        {
            if (screens.TryGetValue(typeof(T), out var screen))
            {
                ActiveScreen?.gameObject.SetActive(false);
                screen.gameObject.SetActive(true);
                screen.Stretch();
                ActiveScreen = screen;
                screen.Display();
            }
        }
    }
}